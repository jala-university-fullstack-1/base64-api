﻿namespace Base64_webapi.Service.Abstraction
{
    public interface IDecode
    {
        string Decode(string input);
    }
}
