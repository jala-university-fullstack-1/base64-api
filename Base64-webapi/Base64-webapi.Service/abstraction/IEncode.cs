﻿namespace Base64_webapi.Service.Abstraction
{
    public interface IEncode
    {
        string Encode(string input);
    }
}
