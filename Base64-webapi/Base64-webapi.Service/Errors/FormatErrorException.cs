namespace Base64_webapi.Service.Errors
{
    public class FormatErrorException : Exception, IServiceException
    {
        public ServiceErrorTypes ErrorType => ServiceErrorTypes.InvalidInputError;

        public string ErrorMessage => "Text to decode needs to be in Base-64 Format!";
    }
}