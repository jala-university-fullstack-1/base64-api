namespace Base64_webapi.Service.Errors
{
    public interface IServiceException
    {
        public ServiceErrorTypes ErrorType { get; }
        public string ErrorMessage { get; }
    }
}