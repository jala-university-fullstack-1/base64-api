﻿using Base64_webapi.Service.Abstraction;
using System.Text;

namespace Base64_webapi.Service.Implementation
{
    public class Encode : IEncode
    {
        string IEncode.Encode(string input)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}
