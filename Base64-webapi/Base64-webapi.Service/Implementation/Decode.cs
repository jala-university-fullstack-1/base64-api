﻿using Base64_webapi.Service.Abstraction;
using Base64_webapi.Service.Errors;

namespace Base64_webapi.Service.Implementation
{
    public class Decode : IDecode
    {
        string IDecode.Decode(string input)
        {
            try
            {
                var base64EncodedBytes = Convert.FromBase64String(input);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch (FormatException)
            {
                throw new FormatErrorException();
            } 

        }
    }
}
