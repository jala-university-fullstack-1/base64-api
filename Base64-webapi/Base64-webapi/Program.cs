using Base64_webapi.Service.Abstraction;
using Base64_webapi.Service.Implementation;
using NLog;
using NLog.Web;
namespace Base64_webapi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            


                var builder = WebApplication.CreateBuilder(args);
            /*
            var logger = new LoggerConfiguration()
            .MinimumLevel.Warning()
            .Enrich.WithSpan()
            .WriteTo.RollingFile(new JsonFormatter(), @"..\logs\log-{Date}.txt")
            .WriteTo.Console()
            .CreateLogger();
            
            builder.Logging.AddSerilog(logger);
            */
            var logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
            logger.Debug("init main");

            builder.Logging.ClearProviders();
            builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
            builder.Host.UseNLog();

            // Add services to the container.

            builder.Services.AddControllers();

            // Adding cors to allow all origins.
            builder.Services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                   b => b.AllowAnyHeader()
                    .AllowAnyOrigin()
                    .AllowAnyMethod());
            });

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();

            builder.Services.AddSwaggerGen();

            builder.Services.AddScoped<IDecode, Decode>();
            builder.Services.AddScoped<IEncode, Encode>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            app.UseHttpsRedirection();

            app.UseCors("AllowAll");

            app.UseExceptionHandler("/error");

            app.UseAuthorization();

            app.MapControllers();

            app.Run();

        }
    }
}