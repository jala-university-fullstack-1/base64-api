using Base64_webapi.Models;
using Base64_webapi.Service.Abstraction;
using Microsoft.AspNetCore.Mvc;

namespace Base64_webapi.Controllers
{
    [ApiController]
    [Route("api/v1/base64")]
    public class Base64Controller : ControllerBase
    {
        private IDecode _decode;
        private IEncode _encode;

        public Base64Controller(IEncode encode, IDecode decode)
        {
            _decode = decode;
            _encode = encode;
        }

        [HttpPost("encode")]
        public Base64Response Encode(Base64Request base64Request)
        {
            string result = _encode.Encode(base64Request.Input);
            Base64Response response = new Base64Response();
            response.Input = result;
            return response;
        }

        [HttpPost("decode")]
        public Base64Response Decode(Base64Request base64Request)
        {
            string result = _decode.Decode(base64Request.Input);
            Base64Response response = new Base64Response();
            response.Input = result;
            return response;
        }
    }
}