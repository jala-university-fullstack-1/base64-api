﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Base64_webapi.Service.Errors;
using System.Diagnostics;
using System.Net;

namespace Base64_webapi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        private readonly ILogger<ErrorsController> _logger;
        public ErrorsController(ILogger<ErrorsController> logger)
        {
            _logger = logger;
        }

        [Route("/error")]
        public IActionResult Error()
        {
            Exception? exception = HttpContext.Features.Get<IExceptionHandlerFeature>()?.Error;
            var traceId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            if (exception is IServiceException serviceException)
            {
                _logger.LogError("Error TraceID: {traceId} Error Message: {serviceException.ErrorMessage}", traceId,serviceException.ErrorMessage);

                if (serviceException.ErrorType == ServiceErrorTypes.InvalidInputError)
                {
                    return Problem(statusCode: (int)HttpStatusCode.BadRequest, title: serviceException.ErrorMessage);
                }
            }
            return Problem(title: exception?.Message);
        }
    }
}