﻿namespace Base64_webapi.Models
{
    public class Base64Response
    {
        public string Input { get; set; } = "";

        public Base64Response()
        {
        }
        public Base64Response(string input)
        {
            Input = input ?? throw new ArgumentNullException(nameof(input));
        }
    }
}
