﻿namespace Base64_webapi.Models
{
    public class Base64Request
    {
        public string Input { get; set; } = "";

        public Base64Request(string input)
        {
            Input = input ?? throw new ArgumentNullException(nameof(input));
        }

        public Base64Request()
        {
        }
    }
}
