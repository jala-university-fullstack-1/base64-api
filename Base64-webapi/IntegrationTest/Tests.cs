using Newtonsoft.Json;
using System.Text;
using Base64_webapi;
using Base64_webapi.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;

namespace IntegrationTest
{
    [TestClass]
    public class Tests
    {
        /// <summary>
        /// This is a test method to check the endpoint encode, it creates a client and make a request,
        /// it evaluates that the response is correct.
        /// </summary>
        [TestMethod]
        public async Task TestEncodeWorkingWell()
        {
            string url = "/api/v1/base64/encode";
            Base64Request request = new Base64Request(){Input = "hello world!"};
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var application = new WebApplicationFactory<Program>();
            var client = application.CreateDefaultClient();
            var response = await client.PostAsync(url, content);
            response.EnsureSuccessStatusCode();
            var jsonResponse = await response.Content.ReadAsStringAsync();
            Base64Response result = JsonConvert.DeserializeObject<Base64Response>(jsonResponse);
            Assert.AreEqual("aGVsbG8gd29ybGQh", result.Input);
        }

        /// <summary>
        /// This is a method to check the endpoint decode, it creates a client and make a request,
        /// it evaluates that the response is correct.
        /// </summary>
        [TestMethod]
        public async Task TestDecodeWorkingWell()
        {
            string url = "/api/v1/base64/decode";
            Base64Request request = new Base64Request() { Input = "aGVsbG8gd29ybGQh" };
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var application = new WebApplicationFactory<Program>();
            var client = application.CreateDefaultClient();
            var response = await client.PostAsync(url, content);
            response.EnsureSuccessStatusCode();
            var jsonResponse = await response.Content.ReadAsStringAsync();
            Base64Response result = JsonConvert.DeserializeObject<Base64Response>(jsonResponse);
            Assert.AreEqual("hello world!", result.Input);
        }

        /// <summary>
        /// This is a test method to check the status code is correct in encode.
        /// </summary>
        [TestMethod]
        public async Task TestEncodeResponseOk()
        {
            string url = "/api/v1/base64/encode";
            Base64Request request = new Base64Request() { Input = "hello world!" };
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var application = new WebApplicationFactory<Program>();
            var client = application.CreateDefaultClient();
            var response = await client.PostAsync(url, content);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        /// <summary>
        /// This is a test method to check the status code is correct in decode.
        /// </summary>
        [TestMethod]
        public async Task TestDecodeResponseOk()
        {
            string url = "/api/v1/base64/decode";
            Base64Request request = new Base64Request() { Input = "aGVsbG8gd29ybGQh" };
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var application = new WebApplicationFactory<Program>();
            var client = application.CreateDefaultClient();
            var response = await client.PostAsync(url, content);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
    }
}