﻿using Base64_webapi.Service.Abstraction;
using Base64_webapi.Service.Implementation;

namespace Base_webapi.UnitTest
{
    /// <summary>
    /// Unit Tests for the Base64-webapi.Service
    /// </summary>
    [TestClass]
    public class UnitTest
    {
        /// <summary>
        /// Test for a simple string to Encode
        /// </summary>
        [TestMethod]
        public void TestEncode()
        {
            string stringToProve = "marte roja";
            IEncode encode = new Encode();
            string encodedString = encode.Encode(stringToProve);
            Assert.AreEqual("bWFydGUgcm9qYQ==", encodedString);
        }

        /// <summary>
        /// Test for a empty string to Encode
        /// </summary>
        [TestMethod]
        public void TestEncodeEmpty()
        {
            string stringToProve = string.Empty;
            IEncode encode = new Encode();
            string encodedString = encode.Encode(stringToProve);
            Assert.AreEqual(string.Empty, encodedString);
        }

        /// <summary>
        /// Test for a simple string to Decode
        /// </summary>
        [TestMethod]
        public void TestDecode()
        {
            string stringToProve = "bWFydGUgcm9qYQ==";
            IDecode decode = new Decode();
            string decodedString = decode.Decode(stringToProve);
            Assert.AreEqual("marte roja", decodedString);
        }

        /// <summary>
        /// Test for a empty string to Decode
        /// </summary>
        [TestMethod]
        public void TestDecodeEmpty()
        {
            string stringToProve = string.Empty;
            IDecode decode= new Decode();
            string decodedString = decode.Decode(stringToProve);
            Assert.AreEqual(string.Empty, decodedString);
        }
    }
}